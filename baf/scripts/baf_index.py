# -*- coding: utf-8 -*-
import os
import sys
import stat

import transaction
from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )
from pyramid.scripts.common import parse_vars

from ..models.meta import Base
from ..models import (
    get_engine,
    get_session_factory,
    get_tm_session,
    FileType,
    FileName,
    FileContent,
    Folder,
    )


def walk_on_files(folder, dbsession, exclusion=None):
    dirname = folder.path
    for dirpath, dirnames, filenames in os.walk(dirname):
        # depth = dirpath.count('/') - start_depth + 1
        if exclusion is not None:
            dirnames[:] = filter(exclusion, dirnames)
            filenames[:] = filter(exclusion, filenames)
        for fname in filenames:
            path = os.path.join(dirpath, fname)
            folder_path = path[len(dirname):-len(fname)].strip('/')
            st = os.stat(path)
            inode = st[stat.ST_INO]
            ctime = st[stat.ST_CTIME]
            mtime = st[stat.ST_MTIME]
            size = st[stat.ST_SIZE]
            uid = st[stat.ST_UID]
            gid = st[stat.ST_GID]

            fn = FileName.from_path_name_and_folder_id(
                dbsession, folder_path, fname, folder.id
            )
            if fn is None:
                ft = FileType.init_from_filename(path)
                other = FileType.from_other(dbsession, ft)
                if other is None:
                    dbsession.add(ft)
                else:
                    ft = other
                with open(path, 'rb') as f:
                    signature = FileContent.hash(f.read())
                other_content = FileContent.from_signature(
                    dbsession, signature
                )
                if other_content is None:
                    fc = FileContent()
                    fc.signature = signature
                    fc.size = size
                else:
                    fc = other_content
                    assert fc.size == size, "%d != %d" % (fc.size, size)
                dbsession.add(fc)
                fn = FileName(path=folder_path, name=fname)
                fn.folder = folder
                fn.filetype = ft
                fn.filecontent = fc
                fn.inode = inode
                fn.mtime = mtime
                fn.ctime = ctime
                fn.size = size
                fn.uid = uid
                fn.gid = gid
                dbsession.add(fn)
                print(path)
                print('  %s' % str(ft))
            else:
                pass
                # TODO: c'est la partie intéressante...


def exclusion_f(x):
    exclude = {'__pycache__'}
    return (not x.startswith('.')
            and x not in exclude
            and not x.endswith('.egg-info')
            and not x.endswith('~')
            and not x.endswith('.pyc'))


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> <folder_to_index> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 3:
        usage(argv)
    config_uri = argv[1]
    folder_to_index = argv[2]
    options = parse_vars(argv[3:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)

    print("INDEX: %s" % folder_to_index)

    engine = get_engine(settings)
    Base.metadata.create_all(engine)
    session_factory = get_session_factory(engine)
    #
    # Folder path
    #
    if not folder_to_index.startswith('/'):
        folder_to_index = os.path.abspath(folder_to_index)
    #
    # Create Folder
    #
    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)
        folder = Folder(path=folder_to_index)
        dbsession.add(folder)
    #
    # Index files inside Folder
    #
    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)
        folder = Folder.from_path(dbsession, folder_to_index)
        walk_on_files(folder, dbsession, exclusion_f)
