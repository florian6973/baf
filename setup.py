import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'plaster_pastedeploy',
    'pyramid',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'waitress',
    'alembic',
    'pyramid_retry',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    #
    'python-magic-bin ; platform_system=="Windows"', # Windows binary for python-magic
    'python-magic',  # OSX: brew install libmagic
                     # https://pypi.org/project/python-magic/
                     # https://github.com/ahupp/python-magic
    # 'filetype',  # https://github.com/h2non/filetype.py
    # 'fleep',  # https://github.com/floyernick/fleep-py
    'chardet',
]

tests_require = [
    'WebTest',
    'pytest',
    'pytest-cov',
]

setup(
    name='baf',
    version='0.0',
    description='baf',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    python_requires='<3.12', # venusian not supported 3.12 find_module
    author='',
    author_email='',
    url='',
    keywords='web pyramid pylons',
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'paste.app_factory': [
            'main = baf:main',
        ],
        'console_scripts': [
            'baf_index = baf.scripts.baf_index:main',
        ],
    },
)
